import {Component} from 'react';
import {ItunesMusicInterface} from "../../interfaces/itune-music.interface";
import {Subscription, timer} from "rxjs";
import {ValuesCentralService} from "../../services/values-central.service";
import {AppFilters} from "../../filters/AppFilters";

interface IProps {
  suggestedTunes: Partial<ItunesMusicInterface>[]
}

interface IAppState {
  rotatingTunesList: Partial<ItunesMusicInterface>[];
  searchResultList: Partial<ItunesMusicInterface>[];
}

const defaultList: Partial<ItunesMusicInterface>[] = [
  {collectionName: 'A', artworkUrl100: '',},
  {collectionName: 'B', artworkUrl100: '',},
  {collectionName: 'C', artworkUrl100: '',},
  {collectionName: 'D', artworkUrl100: '',},
  {collectionName: 'E', artworkUrl100: '',},
];

export class IntermittentRotatingList extends Component {
  public state: IAppState;
  private readonly tunesRotatingTimer$ = timer(1000, 1000);
  private tunesRotatingSubscription!: Subscription;
  private vcService!: ValuesCentralService;
  private readonly highlightKeywordPipe: ((itemValue: string, activeKeyword: string) => string);
  private lastKeyword = '';

  constructor(props: IProps) {
    super(props);
    this.vcService = ValuesCentralService.getInstance();
    this.tunesRotatingTimer$ = timer(1000, 1000);
    this.highlightKeywordPipe = AppFilters.highlightKeywordPipe;
    this.state = {
      rotatingTunesList: defaultList,
      searchResultList: [],
    }
  }

  componentDidMount() {
    this.vcService.searchTermStream$.subscribe((keyword) => {
      this.lastKeyword = keyword
    });
    this.vcService.searchResultsSignal.subscribe((tunes) => {
      this.setState({
        searchResultList: tunes,
      });
    });

    this.handleRotatingTimer();
  }

  componentWillUnmount(): void {
    if(this.tunesRotatingTimer$){
      this.tunesRotatingSubscription.unsubscribe();
    }
  }


  handleRotatingTimer() {
    this.tunesRotatingSubscription = this.tunesRotatingTimer$.subscribe(() => {
      let replaceableItem: Partial<ItunesMusicInterface>;
      const extract = [...this.state.rotatingTunesList];
      const results = [...this.state.searchResultList];
      if (this.state.searchResultList?.length > 0) {
        replaceableItem = results.splice(0, 1)[0];
        extract.splice(0, 1);
        this.setState({
          searchResultList: [...results, replaceableItem],
        });
      } else {
        replaceableItem = extract.splice(0, 1)[0];
      }
      extract.push(replaceableItem);
      this.setState({
        rotatingTunesList: extract,
      })
    });
  }

  getHighlightedKeywordHtml(collectionName: string): string {
   return this.highlightKeywordPipe(collectionName, this.lastKeyword);
  }

  render() {
    const getSearchResultsUI = (): JSX.Element[] => {
      return this.state?.rotatingTunesList.map((album: Partial<ItunesMusicInterface>, i: number) => {
        const albumPix = AppFilters.itemPixOrDefault(album?.artworkUrl100?.trim() || null);
        return (
            <a className={'suggested-songs__song'}
               href={album?.collectionViewUrl ?? '#'}
               target='_blank'
               rel="noreferrer"
               key={`Album_Key_${i}`}>
              <span className={'suggested-songs__song-pix-box'}>
                <img src={albumPix}
                     alt={album.collectionName}
                     aria-label={album.collectionName}
                     className={'suggested-songs__song-pix'}/>
              </span>
              <span className={'suggested-songs__song-keyword-highlighted'}>
                <span className={'suggested-songs__song-keyword-highlighted--text'} dangerouslySetInnerHTML={{__html: this.getHighlightedKeywordHtml(album?.collectionName as string)}}></span>
              </span>
            </a>
        );
      });
    };
    return (<div className="suggested-songs">{getSearchResultsUI()}</div>);
  }
}
