import React, {Component} from "react";
import {IntermittentRotatingList} from "../../intermittent-rotating-list/";
import {debounceTime, Subject} from "rxjs";
import {ValuesCentralService} from "../../../services/values-central.service";
import {SearchBrokerService} from "../../../services/search-broker.service";
import {ItunesMusicInterface} from "../../../interfaces/itune-music.interface";

interface EventKeywordInterface {
  event: KeyboardEvent | Event;
  keyword: string;
}

interface IAppState {
  searchTerm: string;
  [key: string]: any;
}

interface IProps {
  [key: string]: any;
}
export class SearchBox extends Component {
  public state: IAppState;
  private appRefs = {
    keyword: React.createRef<HTMLInputElement>(),
    resetIcon: React.createRef(),
    labelTextNode: React.createRef(),
    searchResultsComponent: React.createRef(),
  };

  private shouldRenderResult = true;
  private searchBrokerService!: SearchBrokerService;
  private vcService!: ValuesCentralService
  public suggestedTunes: ItunesMusicInterface[] = [];
  public shouldFetchData = true;
  private subject: Subject<EventKeywordInterface>;

  constructor(props: IProps) {
    super(props);
    this.state = {
      searchTerm: '',
    }
    this.subject = new Subject<EventKeywordInterface>();
    this.vcService = ValuesCentralService.getInstance();
    this.searchBrokerService = SearchBrokerService.getInstance();
  }

  componentWillMount(): void {
    this.subject.pipe(
        debounceTime(500)
    ).subscribe(eKeyword => {
      this.fetchSuggestedTunes(eKeyword.event as KeyboardEvent, eKeyword.keyword);
    });
  }

  fetchSuggestedTunes(event: KeyboardEvent, keyword: string): void {
    switch (event.key) {
      case 'Esc': // IE/EDGE SPECIFIC VALUE
      case 'Escape':
        if(this.appRefs.keyword?.current){
          this.resetSearchField(this.appRefs.keyword?.current);
        }
        break;

      case "Enter":
        this.fetchResults(keyword);
        break;

      default:
    }
  }

  fetchResults(keyword: string): void {
    this.shouldFetchData = true;
    this.shouldRenderResult = true;
    keyword = keyword.trim();
    this.suggestedTunes = [];

    this.searchBrokerService.fetchSuggestionsByKeyWord(keyword)
        .subscribe(songs => {
          this.suggestedTunes = songs;
          let lastSearch = '';
          if(this.appRefs.keyword?.current) {
            lastSearch = this.appRefs.keyword.current.value;
            this.resetSearchField(this.appRefs.keyword?.current);
            this.appRefs.keyword.current.value = lastSearch;
          }
        });
  }

  resetSearchField(keywordField: HTMLInputElement): void{
    keywordField.value = '';
    this.shouldFetchData = false;
    this.shouldRenderResult = false;
    this.suggestedTunes = [];
  }

  repositionLabel(labelTextNode: HTMLElement , actionType: string): void{
    switch (actionType.toLowerCase()){
      case 'blur':
        if (labelTextNode.classList.contains('search-label__label-text--entered')) {
          if (this.appRefs.keyword?.current && !this.appRefs.keyword?.current.value){
            labelTextNode.classList.remove('search-label__label-text--entered');
          }
        }
        break;
      case 'focus':
        if (!labelTextNode.classList.contains('search-label__label-text--entered')) {
          labelTextNode.classList.add('search-label__label-text--entered');
        }
        break;
    }
  }

  onKeyUp(event: KeyboardEvent, keyword: string): void {
    this.subject.next({event, keyword});
  }
  render() {
    return (
        <div className="form-wrapper">
          {/* THIS FORM HERE IS ONLY FOR SEMANTIC/ACCESSIBILITY REASONS - IT IS NOT USED ANYWHERE WITHIN THE CODE... */}
          <form className="search-form" id="search-form" onSubmit={ (e) => {e.preventDefault(); } }>
            <label className="search-label" htmlFor="search-field" id="search-field-label">
            <span className="search-label__label-text"
                  ref={this.appRefs.labelTextNode as any}>Please, enter search term:</span>
              <input type="text"
                     onInput={ (e) => {
                       e.preventDefault();
                       this.setState({
                         searchTerm: (e.target as HTMLInputElement).value
                       });
                     }}
                     onKeyUp={ (event) => {
                       this.onKeyUp((event as unknown as KeyboardEvent), this.state.searchTerm );
                     }}
                     onFocus={() => {
                       this.repositionLabel((this.appRefs.labelTextNode.current as HTMLSpanElement), 'focus')
                     }}
                     onBlur={() => {
                       this.repositionLabel((this.appRefs.labelTextNode.current as HTMLSpanElement),'blur')
                     }}
                     className="form-control search-field"
                     id="search-field"
                     autoComplete="off"
                     name="searchKeyword"
                     ref={this.appRefs.keyword as any}/>
              <span className="search-reset-icon"
                    onClick={() => {
                      this.resetSearchField(this.appRefs?.keyword?.current as HTMLInputElement)
                    }}
                    ref={this.appRefs.resetIcon as any}>&times;</span>
            </label>
          </form>

          <aside className="search-results item-search-and-detail-block__detail-block">
            <IntermittentRotatingList></IntermittentRotatingList>
          </aside>

        </div>
    );
  }
}
