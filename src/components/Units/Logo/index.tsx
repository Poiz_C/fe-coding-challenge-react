import logo from '../../../assets/images/poiz-logo.png';

export function Logo() {
  return (
      <figure className="header-logo-figure" role="figure">
        <img className="header-logo-figure__logo" src={logo} alt="Poiz Logo"/>
      </figure>
  );
}
