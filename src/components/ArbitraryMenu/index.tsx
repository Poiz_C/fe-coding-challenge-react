import {MenuInterface} from "../../interfaces/MenuInterface";
import {MenuCollection} from "../../static/MenuCollection";


export function ArbitraryMenu() {
  return (
      <menu className="header-top-menu">
        {
          MenuCollection.map( (menuItem: MenuInterface) => {
            return (
                <li className="header-top-menu__item" key={menuItem.tabIndex}>
                  <a className={ menuItem.cssClasses?.join(' ') }
                     href={ menuItem.linkURL }
                     tabIndex={menuItem.tabIndex}
                     role="link"
                     aria-label={menuItem.label}
                     onClick={(e) => { e.preventDefault();}}
                  >{menuItem.label}</a>
                </li>
            );
          })
        }
      </menu>
  );
};
