import { ArbitraryMenu } from "../ArbitraryMenu";
import { SearchIcon } from "../Units/SearchIcon";
import { Logo } from "../Units/Logo";

export function Header() {
  return (
      <header className="header-main">
        <Logo />
        <ArbitraryMenu />
        <SearchIcon />
      </header>
  );
}
