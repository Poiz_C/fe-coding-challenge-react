import { SearchBox } from "../../Search/SearchBox";
import bgImage from '../../../assets/images/poiz-coding-challenge-pix.jpg';
import {useEffect, useState} from "react";
import {ValuesCentralService} from "../../../services/values-central.service";

export function SearchView() {
  const vcService: ValuesCentralService = ValuesCentralService.getInstance();
  const [shouldDisplay, setShouldDisplay] = useState<boolean>(true);

  useEffect(() => {
    vcService.searchBlockVisibilityStream.subscribe((val) => {
      setShouldDisplay(val);
    })
  })
  const renderIfViewBlockShouldShow = () => {
    return shouldDisplay ? (
        <div className="item-search-and-detail-block">
          <aside className="item-search-and-detail-block__search-block">
            <SearchBox />
          </aside>

          <section className="superfluous-content-block">
            <h2 className="superfluous-content-block__title">
              Front-End Coding Challenge [ React ] - <span className="superfluous-content-block--small">A Solution from Poiz.</span>
            </h2>
            <div className="superfluous-content-block__pix-holder" style={ {backgroundImage: `url('${bgImage}')`}}>
            </div>

            <p className="superfluous-content-block__text" role="presentation">
              This is my Solution to the Front-End Coding Challenge. I took the liberty to implement the same Solution in React - just for Demonstration purposes.
              Again, (<em>like in the Angular Counterpart</em>), no <strong> User Interface [UI]</strong> is provided for kicking off the
              search. Just hit <strong>ENTER</strong> and you are good to go.
              <br/>
              At the Top right corner of the Page is a <strong>Bonus Search Icon</strong>. Click on it to see what
              happens 😃
            </p>
          </section>
        </div>
    ) : null;
  };
  return (
      renderIfViewBlockShouldShow()
  );
}

