import defaultItemPix from '../assets/images/default-pix.png';

export class AppFilters {
  static highlightKeywordPipe(itemValue: string, activeKeyword: string=''): string {
    const keyword = activeKeyword.trim().replace(/([\[\].\-+{}()?\/:])/gi, '\\$1');
    const filterRegExp = new RegExp(keyword, 'ig');
    return  itemValue.replace( filterRegExp, (match ) => {
      return match ? `<span class="emphasized">${match}</span>` : match;
    });
  }

  static itemPixOrDefault(pixSrc: string|undefined|null): string {
    return pixSrc?.trim() as string ?? defaultItemPix;
  }

}
