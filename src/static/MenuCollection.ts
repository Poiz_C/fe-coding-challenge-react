import {MenuInterface} from "../interfaces/MenuInterface";

const baseMenu: MenuInterface =  {
  label: 'Search Tunes',
  linkType: 'Internal',
  linkURL: '/',
  linkName: 'none',
  tabIndex: 10000,
  cssClasses: ['header-top-menu__link', 'header-top-menu__link--disabled']
};

export const MenuCollection: MenuInterface[] = [
  {
    ...baseMenu,
    label: 'Search Tunes',
    tabIndex: 10001,
    cssClasses: ['header-top-menu__link', 'header-top-menu__link--active']
  },
  {
    ...baseMenu,
    label: 'Download Free Tunes',
    tabIndex: 10002,
  },
  {
    ...baseMenu,
    label: 'Browse Our Tunes Collection',
    tabIndex: 10003,
  },
];
