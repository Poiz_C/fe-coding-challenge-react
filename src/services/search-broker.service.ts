import {map, Observable, of, shareReplay} from 'rxjs';
import {ItunesMusicInterface, ItunesPayloadInterface} from "../interfaces/itune-music.interface";
import {ValuesCentralService} from "./values-central.service";
import {ajax, AjaxConfig} from "rxjs/ajax";

export class SearchBrokerService {
  tunesEndpoint = 'https://itunes.apple.com/search';
  private vcService!: ValuesCentralService;
  private static instance: SearchBrokerService;

  private constructor() {
    if( !SearchBrokerService.instance ){
      this.initialize();
    }
  }

  private initialize() {
    this.vcService = ValuesCentralService.getInstance();
  }

  static getInstance(): SearchBrokerService {
    if(!SearchBrokerService.instance){
      SearchBrokerService.instance = new SearchBrokerService();
    }
    return SearchBrokerService.instance;
  }

  /**
   * GIVEN A KEYWORD REPRESENTING AN ITEM'S FULL OR PARTIAL NAME,
   * FETCHES A COLLECTION OF ITEMS [SONGS] FROM THE SPECIFIED ENDPOINT... [ `this.tunesEndpoint` ]
   */
  fetchSuggestionsByKeyWord(keyword: string): Observable<ItunesMusicInterface[]> {
    keyword = keyword.trim();
    // BROADCAST LAST ENTERED SEARCH-TERM/KEYWORD
    this.vcService.broadcastSearchTerm(keyword);
    // BAIL EARLY IF WE DON'T HAVE A KEYWORD TO AVOID FETCHING ENTIRE PAYLOAD.
    // HOWEVER, THIS METHOD SHOULD ONLY BE CALLED WITH AN EXPLICIT KEYWORD PARAMETER.
    if (!keyword) {
      return of([]);
    }
    // CREATE AJAX CONFIGURATION FOR FETCHING SONGS BY SEARCH-TERM....
    keyword = keyword.replace(/([\[\].\-+{}()?\/:])/gim, '\\$1');
    const config: AjaxConfig = {
      url: this.tunesEndpoint,
      queryParams: {term: keyword},
      method: 'GET',
      responseType: 'json',
    }

    // QUERY THE API AND TRANSFORM THE PAYLOAD USING OBSERVABLES + RXJS...
    return ajax<ItunesMusicInterface[]>(config).pipe(
        shareReplay(),
        map( (data:  {response: ItunesPayloadInterface} | any) => {
          // LIMIT SELECTION TO ALBUMS ONLY (COLLECTION NAME) - ACCORDING TO SPECS...
          const payload = data.response;
          const songsOnly =  payload?.results.filter( (tune: ItunesMusicInterface) => !!tune.collectionName);
          const uniqueSongsObject: {[key: string]: ItunesMusicInterface} = {};

          // REMOVE COLLECTION WITH DUPLICATE COLLECTION NAMES:
          songsOnly.forEach( (song: ItunesMusicInterface) => {
            if(song?.collectionName){
              uniqueSongsObject[song.collectionName] = song;
            }
          });
          const uniqueSongsList: ItunesMusicInterface[] = Object.values(uniqueSongsObject);

          // SORT THE RESULTING COLLECTION ALPHABETICALLY
          uniqueSongsList.sort((a, b) => {
            return (a.collectionName < b.collectionName) ? -1 : 1
          });

          // EXTRACT THE FIRST, TOP 5 ENTRIES FROM THE FILTERED & SORTED LIST...
          const finalResult = uniqueSongsList.slice(0, 5);

          // BROADCAST THE SEARCH RESULTS...
          this.vcService.broadcastSearchResults(finalResult);

          // FINALLY, RETURN ONLY THE FINAL RESULT... ( THE FIRST 5 RECORDS )...
          return finalResult;
        }),
    );

  }
}


