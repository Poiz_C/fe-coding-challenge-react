import {Subject} from 'rxjs';
import {ItunesMusicInterface} from '../interfaces/itune-music.interface';

export class ValuesCentralService {
  private static instance: ValuesCentralService;
  searchTermStream$!: Subject<string>;
  searchResultsSignal!: Subject<ItunesMusicInterface[]>;
  searchBlockVisibilityStream!: Subject<boolean>;

  private constructor() {
    if(!ValuesCentralService.instance){
      this.initialize();
    }
  }

  public static getInstance() {
    if(!ValuesCentralService.instance){
      ValuesCentralService.instance = new ValuesCentralService();
    }
    return ValuesCentralService.instance;
  }

  private initialize() {
    this.searchResultsSignal = new Subject<ItunesMusicInterface[]>();
    this.searchBlockVisibilityStream = new Subject<boolean>();
    this.searchTermStream$ = new Subject<string>();
  }


  broadcastSearchBlockVisibilityStream(val: boolean): void {
    this.searchBlockVisibilityStream.next(val);
  }

  broadcastSearchResults(finalResult: ItunesMusicInterface[]) {
    this.searchResultsSignal.next(finalResult);
  }

  broadcastSearchTerm(searchTerm: string){
    this.searchTermStream$.next(searchTerm);
  }

}
