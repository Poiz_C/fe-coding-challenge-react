import React from 'react';
import './assets/styles/globals.scss';
import {Header} from "./components/Header";
import {SearchView} from "./components/views/searchView";

function App() {
  return (
      <div className="wrapper">
        <Header />
        <div className="main-content">
          <SearchView />
        </div>
      </div>
  );
}
export default App;

