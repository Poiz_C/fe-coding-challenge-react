export interface ItunesMusicInterface {
  wrapperType: string;
  artistId: number;
  collectionId: number;
  artistName: string;
  collectionName: string;
  collectionCensoredName: string;
  artistViewUrl: string;
  collectionViewUrl: string;
  artworkUrl60: string;
  artworkUrl100: string;
  collectionPrice: number;
  collectionExplicitness: string;
  copyright: string;
  country: string;
  currency: string;
  releaseDate: string;
  primaryGenreName: string;
  previewUrl: string;
  description: string;

  // OPTIONALS...
  kind?: string;
  trackId?: number;
  trackName?: string;
  discCount?: number;
  discNumber?: number;
  trackPrice?: string;
  trackCount?: number;
  trackNumber?: number;
  artworkUrl30?: string;
  isStreamable?: boolean;
  trackViewUrl?: string;
  trackTimeMillis?: number;
  trackCensoredName?: string;
  trackExplicitness?: string;
}

export interface ItunesPayloadInterface {
  results: ItunesMusicInterface[];
  resultCount: number;
}
