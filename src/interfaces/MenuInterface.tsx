
export interface MenuInterface {
  label: string,
  linkType: 'Internal' | 'External',
  linkURL?: string,
  linkName?: string,
  tabIndex?: number,
  cssClasses?: string[]
}
