# Front-End Coding Challenge [React]

This ReST-Based Search App is built on top of `Version 18.2.0` of the `React Framework` and was generated with [create-react-app CLI](https://create-react-app.dev/) 

## About the App.

This App simply interfaces with a Remote Service via ReST API to fetch + filter Items ( Songs ) based on user-supplied search Term(s).
The returned Payload is then rendered as described in the Specs.
At first, we have a default Songs collection with `Collection Names` labelled from `A - E`. Without entering any Search Term, default the Entries (Songs) 
would intermittently rotate at 1 second interval... and as soon as a `Keyword` is entered, and you hit the `Enter Key`, 
a call is made to the `Itunes API`. We then de-duplicate the entries from the `Payload`, filter and sort them to be consumed from within the UI.
The Payload is then systematically, added to the Display, replacing the previous entries one-at-a-time every second.

## Setup

After cloning the Repository, enter the repo-directory by running: `cd $path-to-repo`.
The quickest way to see results without needing any elaborate setup is to run the App in dev mode by issuing the following commands on the Terminal.

First install dependencies by running: `yarn install` or `npm install`.  Once the dependencies are resolved, you may want to run: `yarn start` or `npm run start` to start the Dev. Server.

Afterwards, using your preferred Web-Browser, you may navigate to : [http://localhost:3000/](http://localhost:3000/)

Click on the `Search Icon` in the Top-Right corner to `toggle` the
View-Pane and thus expose or blind the Search Input Field as well as the rendered result.... 
This operation does not reinitialize the `App` nor does it the logical data flow within the `UI`...

Start interacting with the App to see if it meets the Standards of the Challenge... (including Code-Reviews - of course)...

## Tests
No Test was written for the App due to `Time constraints`.

## Screenshot 
![Desktop Screenshot: #1](https://gitlab.com/Poiz_C/fe-coding-challenge-react/-/raw/main/src/assets/images/screenshot-desktop-1.png)
![Desktop Screenshot: #2](https://gitlab.com/Poiz_C/fe-coding-challenge-react/-/raw/main/src/assets/images/screenshot-desktop-2.png)
![Mobile Screenshot: #1](https://gitlab.com/Poiz_C/fe-coding-challenge-react/-/raw/main/src/assets/images/screenshot-mobile-1.png)
![Mobile Screenshot: #2](https://gitlab.com/Poiz_C/fe-coding-challenge-react/-/raw/main/src/assets/images/screenshot-mobile-2.png)

